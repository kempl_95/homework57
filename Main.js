'use strict';
//Создайте объект пользователя
function User(id, name, surname, login, email, password, isAuthorized) {
    this.id = id;
    this.name = {
        name: name,
        surname: surname
    };
    this.login = login;
    this.email = email;
    this.password = password;
    this.isAuthorized = isAuthorized;               //Как понять, авторизован он или нет
}
//Создаю объект лайк
function Like(id, user, datetime) {
    this.id = id;
    this.user = user;
    this.datetime = datetime;
}
//Создайте объект поста.
function Post(id, description, datetime, user) {
    this.id = id;
    this.description = description;
    this.datetime = datetime;
    this.user = user;           //Как вы свяжете его с пользователем? - Попробовал связать по объекту, получилась связь. Не уверен в корректности, но работает
    this.like = undefined;
}

//Создайте объект комментария
function Comment(id, text, datetime, post, user) {
    this.id = id;
    this.text = text;
    this.datetime = datetime;
    this.post = post;               //Как бы вы связали его с постом и пользователем? - ну как-то так
    this.user = user;               //Как бы вы связали его с постом и пользователем? - ну как-то так
}

let userNames = ["Kamil", "Ulia", "Milen", "Amal", "Nael"];
let userSurnames = ["Mirzhalalov", "Potapova", "Mirzhalalov", "Arapbaev", "Vaitkus"];
let userLogins = ["kempl95kg", "potapovaUlia", "antAras", "arapbaev88", "ntw98"];

//Создал массив пользователей
let users = [];
function createUsers(){
    for (let i = 0; i< userNames.length; i++){
        users.push(new User(i, userNames[i], userSurnames[i], userLogins[i], userLogins[i].concat("@gmail.com"), i+i+i, false));
    }
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //Максимум не включается, минимум включается
}
//Создайте массив постов
let postQty = 10;
let posts = [];
function createPosts(){
    for (let i = 0; i< postQty; i++){
        posts.push(new Post(i, "description".concat(i.toString()), new Date(), users[getRandomInt(0,users.length)]));
    }
}
//Добавление новый пост в массив
function addNewPost(postArray, usersArray){
    postArray.push(new Post(postArray.length, "description".concat(postArray.length), new Date(), usersArray[getRandomInt(0,usersArray.length)]));
}

function authorizeUser(User){
    User.isAuthorized = true;
}
//Добавление лайка посту
function addLikeToPost(postId){
    for (let i = 0; i < posts.length; i++){
        if (posts[i].id === postId){
            posts[i].like = new Like("1", users[getRandomInt(0,users.length)], new Date());
        }
    }
}
//Удаление лайка у поста
function delLikeFromPost(postId){
    for (let i = 0; i < posts.length; i++){
        if (posts[i].id === postId){
            posts[i].like = undefined;
        }
    }
}

//Создаю объекты, выполняю вышесозданные методы:
createUsers();
createPosts();
let someUser = users[2];                                                                                    //Создайте объект пользователя.
let somePost = new Post("1", "blablabla", new Date(), users[0]);                              //Создайте объект поста.
let someComment = new Comment("1", "textttt", "this is a comment", somePost, someUser);    //Создайте объект комментарий.

//Отображаю в консоли
console.log(someUser);
console.log(somePost);
console.log(someComment);
console.log("-------------------------------");
console.log(users);
console.log(posts);
console.log("---------------Добавил новый пост в массив постов----------------");
addNewPost(posts, users);
console.log(posts);
console.log("---------------Меняю состояние авторизации у рандомного пользователя----------------");
authorizeUser(users[getRandomInt(0, users.length)]);
console.log(users);
console.log("---------------Добавил новый пост в массив постов----------------");
let rnd = getRandomInt(0, posts.length);
console.log("id поста: "+ rnd);
addLikeToPost(rnd);
console.log(posts);
console.log("---------------Удалить пост----------------");
delLikeFromPost(rnd);
console.log(posts);